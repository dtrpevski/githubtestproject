//  Created Darko Trpevski on 3.8.21.
//  Copyright © Darko Trpevski. All rights reserved.
//
//  Template generated by Darko Trpevski
//  Component: Model - GitHubSearchModel

import Foundation

struct GitHubSearchModel : Codable {

        let incompleteResults : Bool?
    var items : [GitHubSearchItem]?
        let totalCount : Int?

        enum CodingKeys: String, CodingKey {
                case incompleteResults = "incomplete_results"
                case items = "items"
                case totalCount = "total_count"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                incompleteResults = try values.decodeIfPresent(Bool.self, forKey: .incompleteResults)
                items = try values.decodeIfPresent([GitHubSearchItem].self, forKey: .items)
                totalCount = try values.decodeIfPresent(Int.self, forKey: .totalCount)
        }

}
