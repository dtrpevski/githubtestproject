//
//  GitHubItemCell.swift
//  GitHubRepo
//
//  Created by Darko Trpevski on 4.8.21.
//

import UIKit

class GitHubItemCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var repositoryNameLabel: UILabel!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    
    // MARK: - Awake from NIB
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    // MARK: - Configure
    func configure(model: GitHubSearchItem) {
        repositoryNameLabel.text = model.fullName
        lastUpdateLabel.text = model.updateDateTime()
    }
}
