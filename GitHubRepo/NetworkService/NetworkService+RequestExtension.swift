//
//  NetworkService+RequestExtension.swift
//  GitHubRepo
//
//  Created by Darko Trpevski on 3.8.21.
//

import UIKit

// MARK: - EndPoint URL's
extension NetworkService {
    
    func url(for type: RequestType, query: String = "") -> URL? {
        switch type {
        case .Search:
            let url = URL(string: "\((Configuration.URLs.baseURL))\(query)")
            return url
        }
    }
}

enum RequestType: String {
    case Search = "search"
}
enum ApiResult {
    case failure(RequestError)
}

enum HTTPMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}
enum RequestError: Error {
    case unknownError
    case connectionError
    case invalidRequest
    case notFound
    case invalidResponse
    case serverError
    case serverUnavailable
}
