//
//  NetworkService.swift
//  GitHubRepo
//
//  Created by Darko Trpevski on 3.8.21.
//

import UIKit
import Combine

class NetworkService {

    // MARK: - Vars
    private var cancalables = Set<AnyCancellable>()
    var type: RequestType = .Search
    
    // MARK: - Init
    init(type: RequestType) {
        self.type = type
    }
    
    // MARK: - Generic get call
    func fetchItems<T: Decodable>(type: RequestType, query: String, completition: @escaping (Result<T, Error>) ->Void) {
       
        guard let url = url(for: type, query: query) else { return }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = HTTPMethod.get.rawValue
        
        URLSession.shared.dataTaskPublisher(for: url)
            .map{ $0.data }
            .decode(type: T.self, decoder: JSONDecoder())
            .sink { (resultCompletition) in
                switch (resultCompletition) {
                case .finished:
                    break
                case .failure(let error):
                    completition(.failure(error))
                }
            }
            receiveValue: { (resultArray) in
                completition(.success(resultArray))
            }.store(in: &cancalables)


    }
}
