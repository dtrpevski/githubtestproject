//
//  UITableView+Extension.swift
//  GitHubRepo
//
//  Created by Darko Trpevski on 5.8.21.
//

import UIKit

extension UITableView {
    
    func items<Element>(_ builder: @escaping (UITableView, IndexPath, Element) -> UITableViewCell) -> ([Element]) -> Void {
        let dataSource = CombineTableViewDataSource(builder: builder)
        return { items in
            dataSource.pushElements(items, to: self)
        }
    }
}
