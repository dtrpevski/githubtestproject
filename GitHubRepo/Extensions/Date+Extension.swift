//
//  Date+Extension.swift
//  GitHubRepo
//
//  Created by Darko Trpevski on 4.8.21.
//

import Foundation

extension Date {
    
    func toString(format: String) -> String {
        let dateDateFormatter = DateFormatter()
        dateDateFormatter.dateFormat = format
        let date = dateDateFormatter.string(from: self)
        return date
    }
}
