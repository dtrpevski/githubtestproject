//
//  String+Extension.swift
//  GitHubRepo
//
//  Created by Darko Trpevski on 4.8.21.
//

import Foundation

extension String {
    
    func toDate(format: String) -> Date {
        let dateDateFormatter = DateFormatter()
        dateDateFormatter.dateFormat = format
        let date = dateDateFormatter.date(from: self) ?? Date()
        return date
    }

}
