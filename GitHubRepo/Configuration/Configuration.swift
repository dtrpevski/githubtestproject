//
//  Configuration.swift
//  GitHubRepo
//
//  Created by Darko Trpevski on 3.8.21.
//

import Foundation

public class Configuration {
    
    // MARK: - URL's
    public struct URLs {
        static let baseURL = "https://api.github.com/search/repositories?q="
    }

}
